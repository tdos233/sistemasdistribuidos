


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;


public class ChandyLamport implements Runnable{

	int porta;
	Processo processo;
	ConectividadeProcessos conectividade = null;
	int maxAmount = 100;
	int serverId;
	int marker = 0,qntQuit=0;
	
	
	
	/**
	 * All initialization takes here.
	 * @throws UnknownHostException
	 */
	public ChandyLamport(String host) throws UnknownHostException {
		
		conectividade = ConectividadeProcessos.getServerPropertiesObject();
		
		processo = new Processo(host);
		serverId = conectividade.processoId.get(host);
                porta=processo.canaisEntrada[serverId];

	}

	
	/**
	 * P0 -> P1 = 0
	 * P1 -> P2 = 1
	 * P2 -> P0 = 2
	 * P1 -> P0 = 3
	 * P0 -> P2 = 4
	 * P2 -> P1 = 5
	 */
	public int recuperarCanal(int sourceProcess, int destProcess){
		if(sourceProcess == 0){
			return (destProcess == 1) ? 0 : 4 ;
		}
		else if(sourceProcess == 1){
			return (destProcess == 0) ? 3 : 1 ;
		}
		else if(sourceProcess == 2){
			return (destProcess == 0) ? 2 : 5 ;
		}
		return -1;
	}

	public String recuperarHost(int process){

		switch(process){
		case 0: 
			return "2,3";
		case 1: 
			return "0,5";
		case 2: 
			return "1,4";

		default:
			return "Invalid.";
		}
	}

	/**
	 * All the main execution happens here.
	 */
	public void run(){
		while(true){
			String threadName = Thread.currentThread().getName();
			if(!processo.primeiro){
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
				processo.primeiro = true;
			}
			if(threadName.equals("local")){
				//Pick a random index of the child.
				Random rand = new Random();
				int randomAmount = rand.nextInt(maxAmount + 1);
				int currentAmount = processo.dinheiro;
				Socket clientSocket = null; 
				int randomProcess = rand.nextInt(conectividade.numeroProcessos);

				try{
					Thread.sleep(1000); 
				}
				catch(Exception e){
					errorMessage(e);
				}


				if(processo.envioMarker){
					try{
						enviandoMarker(serverId, marker);
						processo.envioMarker = false;
					}
					catch(Exception e){
						//errorMessage(e);
						//break;
					}
				}
                                if(processo.quit && !processo.envioMarker){
					try{
                                            if(qntQuit==0){
                                                sendQuit(serverId);
                                                
                                                
                                            }
						break;
					}
					catch(Exception e){
						//errorMessage(e);
						break;
					}
				}

				while(randomProcess == serverId){
					randomProcess = rand.nextInt(conectividade.numeroProcessos);
				}


				String endereco = conectividade.ips[randomProcess];

				if((currentAmount - randomAmount) > 0){

					processo.dinheiro -= randomAmount;
                                        if(processo.quit)break;
					try{
						
						int channelIndex = recuperarCanal(serverId, randomProcess);
						System.out.println("enviando dado para "+ endereco +" dado: " + randomAmount);
						clientSocket = new Socket(endereco, processo.canaisEntrada[randomProcess]);
						DataOutputStream DO = new DataOutputStream(clientSocket.getOutputStream());
						DO.writeInt(channelIndex);
						DO.writeUTF("data");
						DO.writeInt(randomAmount);
                                                clientSocket.close();


					}
					catch (Exception e){
						//e.printStackTrace();
					}
					

				}
				else{
                                    processo.quit=true;
                                    System.err.println("sem dinheiro suficiente");
                                    
				}
				if(serverId == 1 && !processo.envioMarker && !processo.quit){
					try{
						processo.markerCompleto = false;
						Thread.sleep(2000);
						System.out.println("fazendo Snapshot!");
						marker++;
						processo.Estado = processo.dinheiro;
						processo.envioMarker = true;
						processo.primeiroMarker = true;
						processo.estadoGravado = true;
						System.out.println("############################## " + (marker) + " Marker começou ############################## ");
						System.out.println("Marker: " + marker + " Recebido, Processo P" + serverId + " estado: "+ processo.Estado);
						System.out.println("############################## " + (marker) + " Marker terminou ############################## ");



					}
					catch (Exception e) {
						//errorMessage(e);
					}
				}

			}
			else if (threadName.equals("transferController")){

				ServerSocket serverSocket = null;
				Socket clientSocket = null;
                                if(processo.quit){
                                            break;
                                            
                                            
                                        }

				try{
                                    
					serverSocket = new ServerSocket(porta);
					System.out.println("ouvindo na porta: " + porta);

					clientSocket = serverSocket.accept();
					System.out.println("Connectado");
					String data = "";
					DataInputStream DI = new DataInputStream(clientSocket.getInputStream());
					int channelIndex = DI.readInt();
					System.out.println("Index do Canal que recebe: |" + channelIndex + "|");
					data = DI.readUTF();
					System.out.println("Dado recebido: |" + data + "|");


					if(data.equals("marker")){

						
						marker = DI.readInt();
						System.out.println("############################## " + (marker) + " Marker começou ############################## ");
						if(!processo.estadoGravado){
							processo.Estado = processo.dinheiro;
							processo.estadoGravado = true;
							
							processo.enviandoMarker = true;
							System.out.println("Marker: " + marker + " Recebido, Processo P" + serverId + " estado: "+ processo.Estado);
							System.out.println("************ Canal" + channelIndex + " state recorded as:" + processo.canais[channelIndex]);
							processo.canaisReservados[channelIndex] = true;
							processo.envioMarker = true;
						}
						else{
							System.out.println("************ Canal" + channelIndex + " estado gravado como: " + processo.canais[channelIndex]);
							processo.canaisReservados[channelIndex] = true;

							String[] sourceDest = recuperarHost(serverId).split(",");
							int source = Integer.parseInt(sourceDest[0]);
							int dest = Integer.parseInt(sourceDest[1]);
							System.out.println("source: "+ source + " destino:" + dest );
							if(processo.canaisReservados[source] && processo.canaisReservados[dest]){
								System.out.println("Condição verdadeira!");
								processo.markerCompleto = true;
								processo.estadoGravado = false;
								processo.dinheiro += processo.canais[source];
								processo.dinheiro += processo.canais[dest];
								processo.canais[source] = 0;
								processo.canais[dest] = 0;
								processo.canaisReservados[source] = false;
								processo.canaisReservados[dest] = false;
								processo.Estado = 0; 
							}

						}
						System.out.println("############################## " + marker + " Marker terminou ############################## ");

					}
					else if (data.equals("data")){

						int randomAmount = DI.readInt();
						System.out.println("valor recebido: |" + randomAmount + "|");
						if(!processo.estadoGravado){
							System.out.println("depois de receber no Canal" + channelIndex + " dado: " + 0);
							processo.dinheiro += randomAmount;
							System.out.println("Transferencia de  valor: " + randomAmount +" Successo, novo caixa: " + processo.dinheiro);
						}
						else{
							processo.canais[channelIndex] += randomAmount;
							System.out.println("Cnal" + channelIndex + " valor: " + processo.canais[channelIndex]);
						}

					}
                                        else if(data.equals("quit")){
                                            processo.quit=true;
                                            qntQuit=1;
                                           
						
					}
					//					}



				}
				catch(Exception e){
					//errorMessage(e);
				}
				finally{
					try {
						serverSocket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//errorMessage(e);
					}
				}

			}
			/*try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			else{
				break;
			}
		}

	}

	private void enviandoMarker(int markerSource, int marker) throws IOException {
		Socket clientSocket = null;
		int number = 1000;
		for(int server=0; server<conectividade.numeroProcessos; server++){
			if(server!= markerSource){
				try{
					String endereco = conectividade.ips[server];
					int channelIndex = recuperarCanal(markerSource, server);
					clientSocket = new Socket(endereco, processo.canaisEntrada[server]);
					DataOutputStream DO = new DataOutputStream(clientSocket.getOutputStream());
					System.out.println("################ enviando marker para " + endereco + " no Canal" + channelIndex);
					DO.writeInt(channelIndex);
					DO.writeUTF("marker");
					DO.writeInt(marker);
					Thread.sleep(number);
					number += 1000;
				}
				catch(Exception e){
					//e.printStackTrace();
//					break;
				}
				finally{
					clientSocket.close();
				}

			}

		}
		processo.enviandoMarker = false;
		/*System.out.println("Sleeping for 2 sec");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

	}
        private void sendQuit(int markerSource) throws IOException {
		Socket clientSocket = null;
		int number = 2000;
		for(int server=0; server<conectividade.numeroProcessos; server++){
			if(server!= markerSource){
				try{
					String endereco = conectividade.ips[server];
					int channelIndex = recuperarCanal(markerSource, server);
					clientSocket = new Socket(endereco, processo.canaisEntrada[server]);
					DataOutputStream DO = new DataOutputStream(clientSocket.getOutputStream());
					System.out.println("################ enviando quit para " + endereco + " no Canal" + channelIndex);
					DO.writeInt(channelIndex);
					DO.writeUTF("quit");
					DO.writeInt(1);
					Thread.sleep(number);
					
					number += 1000;
                                        qntQuit=1;
				}
				catch(Exception e){
					//e.printStackTrace();
//					
				}
				finally{
					clientSocket.close();
				}

			}

		}
		

	}

	private void errorMessage(Exception e) {
		//e.printStackTrace();
	}

	

}
