

import java.util.HashMap;

public class ConectividadeProcessos {
	
	static ConectividadeProcessos Object = null;
	
	static HashMap<String, Integer> processoId = new HashMap<>();
	static String[] ips = {"127.0.0.1", "127.0.0.2","127.0.0.3"};
	static final int numeroProcessos = ips.length;
	
	private ConectividadeProcessos(){
		int number = 0;
		for(String serverName : ips){
			processoId.put(serverName, number++);
		}
	}
	
	static ConectividadeProcessos getServerPropertiesObject(){
		if(Object == null){
			new ConectividadeProcessos();
		}
		return Object;
	}
	
}
